# snapsen-example-songbook

An example of a repository containg songbook data for use with
[Snapsen](https://gitlab.com/teknologforeningen/snapsen/). This repository can
be used to help you set up your own Snapsen instance with your own material.

## Building and running the example Dockerfile

The [Dockerfile](./Dockerfile) is based on images from the [Snapsen container
registry](https://gitlab.com/teknologforeningen/snapsen/container_registry). To
build and serve the example songbook, simply run the following command inside
this directory:

```bash
docker build -t snapsen-example-image . \
&& docker run --rm -i -p 8000:8000 -p 8443:8443 -i snapsen-example-image
```

After the image has finished building and the container has started you can
visit the site at http://localhost:8000 using HTTP, or at https://localhost:8443
using HTTPS (with self-signed certificates).

For creating your own song book, simply add files to the `./content` directory
and edit the `config.yml` file to customize the settings of Snapsen.

## Dockerfile content

The Dockerfile only needs three instructions to create a custom Snapsen image:

```Dockerfile
FROM registry.gitlab.com/teknologforeningen/snapsen:latest AS snapsen
```

First we start from the Snapsen base image. This image contains the Snapsen
source files and all its dependencies already installed. It also contains two
[ONBUILD](https://docs.docker.com/engine/reference/builder/#onbuild)
instructions, which will be executed when building the current image:

- Copy all files and directories from the current directory to `/snapsen`
  inside the base image.
- Run `gatsby build` inside the `/snapsen` directory.
  This will build the Gatsby site, including the content copied from the current
  directory.

After generating the static content we begin a new stage based on the [Snapsen
nginx
image](https://gitlab.com/teknologforeningen/snapsen/container_registry/3215093).
This image is a standard Alpine-based nginx image with a config file added. It
configures nginx to serve the `/snapsen/public` directory.

```Dockerfile
FROM registry.gitlab.com/teknologforeningen/snapsen/nginx:latest
```

Now we only need to copy the static files built in the first stage to the
current image.

```Dockerfile
COPY --from=snapsen /snapsen/public /snapsen/public
```
