---
type: spirits
melody: Näckens polska
melodyCredit: Arvid August Afzelius
lyricsCredit: trad.
sheet: "./djupt-i-magen.mxl"
audio:
  - name: MIDI
    voices:
      - name: Tenor
        file: "./t1.mp3"
      - name: Baritone
        file: "./t2.mp3"
---

Djupt i magen
=============

Djupt i magen under revbensspjällen,
helan vilar i djupan sal.
Genom krävan snabb liksom gasellen
halvan ilar så skön och sval.
Skälvande lik en till altar gången brud,
hela kroppen väntar på det ljuva ljud,
likt en suck -- genom skrovet går
:repeat[när lilla tersen till blindtarmen når!]

Sjukt i magen efter gårdagskvällen,
skallen lider de värsta kval,
rummet snurrar runt likt karusellen,
uti krävan finns berg och dal.
Räddningen i flaska fort till läppen förs,
hälls däri och genast kroppens lättnad hörs,
likt en suck — genom skrovet går
:repeat[när ägglikören till blindtarmen når!]
