---
type: ceremonial
melody: Kungl. Södermanlands regementes march
lyricsCredit: Erik Hedman
sheet: ./teknologvisan.mxl
audio:
  - name: MIDI
    file: ./teknologvisan.mp3
---

Teknologvisan
=============

Här ska ni se ett schack
som aldrig tappar takt, faderalla.
Teknologer vi äro allihop,
med vin och kvinnor vi stå på en förtrolig fot.
Med tofs i mössan och sticka[^1] i vår hand,
jobba vi flitigt
dock helst uti ett vingårdsland.
På exkursioner och fester e’ vi med.
Teolog e’ man ej
och vi tacka vår lycka för de’.
::repeat[Var teknolog
han är en vingårdens man.
Varhelst i staden man såg
ja där rantar han,
faderampampampampampam[^2]
Lustgårdens frukt,
den plockar han
med vana händer[^3].
Korta krikon, långa krikon,
tjocka krikon, trånga krikon
plockar han med lust
och ett i sänder.]
_1 2 3_ HEJ!

> Denna sång tas lämpligen in av nationens kurator eller styrelseordförande.

[^1]: snabbkalkylator med tunga och slid
[^2]: trummas på bordet
[^3]: på frackfest sjungs “med vita handskar på” Denna sång tas lämpligen in av nationens kurator eller styrelseordförande.
