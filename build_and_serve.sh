#!/bin/sh

IMAGE_REGISTRY=registry.gitlab.com/teknologforeningen/snapsen
docker pull $IMAGE_REGISTRY:latest
docker pull $IMAGE_REGISTRY/nginx:latest
docker build -t snapsen-example .
docker run --rm -i -p 8000:8000 -p 8443:8443 -i snapsen-example
