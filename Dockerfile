# Use `--build-arg TAG_VERSION='vX.X.X'` when building the image to use a
# specific version of Snapsen instead of the latest one.
ARG TAG_VERSION="latest"

FROM registry.gitlab.com/teknologforeningen/snapsen:${TAG_VERSION} AS snapsen

FROM registry.gitlab.com/teknologforeningen/snapsen/nginx:${TAG_VERSION}

COPY --from=snapsen /snapsen/public /etc/nginx/html
